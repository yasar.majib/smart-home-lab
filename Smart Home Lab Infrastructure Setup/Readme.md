Prepare system
    Update reporsitories and upgrade OS to latest packages
        sudo apt update && sudo apt upgrade
    Install Docker
        sudo apt install docker -y
    Install Network Packet Capture
        sudo apt install tshark -y
    Install MQTT Server and Clients
        sudo apt install mosquitto mosquitto_clients -y
    Install Git
        sudo apt install git -y
    Install Java Runtime Environment (required for OpenHAB and Kafka)
        sudo apt install default-jre
    Install OpenHABv3
        git clone -b openHAB3 https://github.com/openhab/openhabian.git /opt/openhabian
        ln -s /opt/openhabian/openhabian-setup.sh /usr/local/bin/openhabian-config
        cp /opt/openhabian/build-image/openhabian.conf /etc/openhabian.conf
        openhabian-config unattended
    HomeAssistant (HomeAssistant in docker)
        docker run -d --name homeassistant -p 8123:8123 --privileged --restart=unless-stopped -e TZ=Etc/Greenwich -v /home/smarthomelab/homeassistant/config:/config --network=host ghcr.io/home-assistant/homeassistant:stable
Kafka (via github)
    Install Kafka
        sudo adduser kafka
        sudo adduser kafka sudo
        su -l kafka
        mkdir ~/Downloads
        Go to https://downloads.apache.org/kafka go to latest version directory and copy the file link and paste {{ FileLink }}
        curl "{{ Latest FileLink }}" -o ~/Downloads/kafka.tgz
        mkdir ~/kafka && cd ~/kafka
        tar -xvzf ~/Downloads/kafka.tgz --strip 1
        nano ~/kafka/config/server.properties
            add these lines
                delete.topic.enable = true
                log.dirs=/home/kafka/logs
        sudo nano /etc/systemd/system/zookeeper.service
            add these lines in the file and save using Ctrl+X -> y -> Enter
                [Unit]
                Requires=network.target remote-fs.target
                After=network.target remote-fs.target

                [Service]
                Type=simple
                User=kafka
                ExecStart=/home/kafka/kafka/bin/zookeeper-server-start.sh /home/kafka/kafka/config/zookeeper.properties
                ExecStop=/home/kafka/kafka/bin/zookeeper-server-stop.sh
                Restart=on-abnormal

                [Install]
                WantedBy=multi-user.target
        sudo nano /etc/systemd/system/kafka.service
            add these lines in the file and save using Ctrl+X -> y -> Enter
                [Unit]
                Requires=zookeeper.service
                After=zookeeper.service

                [Service]
                Type=simple
                User=kafka
                ExecStart=/bin/sh -c '/home/kafka/kafka/bin/kafka-server-start.sh /home/kafka/kafka/config/server.properties > /home/kafka/kafka/kafka.log 2>&1'
                ExecStop=/home/kafka/kafka/bin/kafka-server-stop.sh
                Restart=on-abnormal

                [Install]
                WantedBy=multi-user.target
        Start both Services (ZooKeeper and Kafka)
            sudo systemctl start zookeeper
            sudo systemctl status zookeeper
            sudo systemctl start kafka
            sudo systemctl status kafka
        Enable both Services
            sudo systemctl enable zookeeper
            sudo systemctl enable kafka
    Kafka User Interface (docker)
        docker run --name kafka_ui -p 8181:8080 -e KAFKA_CLUSTERS_0_NAME=LocalCluster -e KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS=localhost:9092 --network=host -d provectuslabs/kafka-ui:latest
Create Kafka Topics (command line / using KafkaUI)
    Using Command Line
        Go to /home/kafka -> Run the following command
            ~/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:2181 --replication-factor 1 --partitions 1 --topic Wireshark
            ~/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:2181 --replication-factor 1 --partitions 1 --topic OpenHAB
    Using Kafka User Interface
        Open http://localhost:8181
Start Network Capturing and Stream to Kafka using Producer 
Configure HomeAssistant to use Kafka
Stream OpenHABv3 event log to Kafka via MQTT
